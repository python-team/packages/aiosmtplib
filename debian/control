Source: aiosmtplib
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Pierre-Elliott Bécue <peb@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-aiosmtpd,
               python3-all,
               python3-hatchling,
               python3-hypothesis,
               python3-pytest,
               python3-pytest-asyncio,
               python3-sphinx,
               python3-sphinx-autodoc-typehints,
               python3-sphinx-copybutton,
               python3-trustme,
               python3-uvloop,
               furo,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://aiosmtplib.readthedocs.io
Vcs-Browser: https://salsa.debian.org/python-team/packages/aiosmtplib
Vcs-Git: https://salsa.debian.org/python-team/packages/aiosmtplib.git

Package: python3-aiosmtplib
Architecture: all
Depends:
 python3-uvloop,
 ${misc:Depends},
 ${python3:Depends},
Suggests: python3-aiosmtplib-doc
Description: Python3 asynchronous SMTP client for use with asyncio
 This is a SMTP client, but written to be based on asyncio for Python3.
 It is designed to work with standard EmailMessages (or MIMEMultipart, or any
 raw message), generated with the standard Python3 library and send these using
 the classic asyncio mechanics.
 .
 It supports TLS/SSL, STARTTLS and authentication.
 .
 This package contains the Python3 library.

Package: python3-aiosmtplib-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Section: doc
Description: Documentation for python3-aiosmtplib
 This is a SMTP client, but written to be based on asyncio for Python3.
 It is designed to work with standard EmailMessages (or MIMEMultipart, or any
 raw message), generated with the standard Python3 library and send these using
 the classic asyncio mechanics.
 .
 It supports TLS/SSL, STARTTLS and authentication.
 .
 This package contains the documentation of the library.
